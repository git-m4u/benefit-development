#!/bin/bash

echo 'Cloning benefit-gateway'
git clone git@bitbucket.org:git-m4u/benefit-gateway.git

echo 'Cloning iso-connector-api'
git clone git@bitbucket.org:git-m4u/iso-connector-api.git

echo 'Generate .env file'
cp benefit-gateway/.env.sample benefit-gateway/.env
