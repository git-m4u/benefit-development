# benefit-development

# Init project
```
$ sh init.sh
```

## Run
```
$ docker-compose up -d
```
## Create and apply migrations
```
$ docker-compose exec app npx prisma migrate dev
```
## Adding dependencies to a package.json file
```
$ docker-compose exec app npm install PACKAGE_NAME
```
## Fetch the logs
```
$ docker-compose logs app
$ docker-compose logs kafka
$ docker-compose logs postgres
$ docker-compose logs iso
```